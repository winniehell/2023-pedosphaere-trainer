// definitely not lodash

export const first = <T = any>(array: Array<T>) => array[0];

export const formatNumber = (value: number, unit?: string, fractionDigits: number = 2) => {
	const formattedNumber = new Intl.NumberFormat('de-DE', {
		maximumFractionDigits: fractionDigits,
		minimumFractionDigits: fractionDigits,
		useGrouping: false
	}).format(value);

	if (unit) {
		return `${formattedNumber} ${unit}`;
	}

	return formattedNumber;
};

export const last = <T = any>(array: Array<T>) => array[array.length - 1];

export const random = (lower: number, upper: number) => lower + (upper - lower) * Math.random();

export const sample = <T = any>(collection: Array<T>) =>
	collection[Math.floor(Math.random() * collection.length)] as T;

export const sum = (array: Array<number>) => array.reduce((a, b) => a + b, 0);

export const arithmeticMean = <T = any>(
	items: Array<T>,
	getValue: (item: T) => number,
	getWeight: (item: T) => number
) => {
	const totalWeight = sum(items.map(getWeight));
	return sum(items.map((i) => getWeight(i) * getValue(i))) / totalWeight;
};

export const harmonicMean = <T = any>(
	items: Array<T>,
	getValue: (item: T) => number,
	getWeight: (item: T) => number
) => {
	const totalWeight = sum(items.map(getWeight));
	return totalWeight / sum(items.map((i) => getWeight(i) / getValue(i)));
};
