import { random, sample } from '../highscore';

interface Stechzylinder {
	festsubstanzdichteInGProCm3: number;
	feuchtMasseInG: number;
	lagerungsdichteInGProCm3: number;
	porositaet: number;
	trockenMasseInG: number;
	volumenInCm3: number;
	volumetrischerLuftgehalt: number;
	volumetrischerWassergehalt: number;
}

export const createStechzylinderProbe = (): Stechzylinder => {
	const volumenInCm3 = sample([10, 25, 50, 100, 250]);
	const wasserMasseInG = Math.round(random(volumenInCm3 / 3, volumenInCm3 / 2));
	const festsubstanzdichteInGProCm3 = random(0, 1) < 0.5 ? 2.65 : sample([1.2, 1.4, 3.0]);
	const volumetrischerWassergehalt = wasserMasseInG / volumenInCm3;
	const volumetrischerLuftgehalt = Math.round(random(10, 30)) / 100;
	const porositaet = volumetrischerWassergehalt + volumetrischerLuftgehalt
	const lagerungsdichteInGProCm3 = festsubstanzdichteInGProCm3 * (1 - porositaet);
	const trockenMasseInG = lagerungsdichteInGProCm3 * volumenInCm3
	const feuchtMasseInG = trockenMasseInG + wasserMasseInG;
	return {
		festsubstanzdichteInGProCm3,
		feuchtMasseInG,
		lagerungsdichteInGProCm3,
		porositaet,
		trockenMasseInG,
		volumenInCm3,
		volumetrischerLuftgehalt,
		volumetrischerWassergehalt
	};
};
