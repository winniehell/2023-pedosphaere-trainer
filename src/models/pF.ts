import { random } from '../highscore';

const erdbeschleunigungInMProS2 = 9.81;
const hekto = 100;
const wasserdichteInKgProm3 = 1000;

export const createPF = () => {
	const pF = Math.round(random(10, 40)) / 10;
	const matrixpotenzialInCm = -Math.pow(10, pF);
	const chemischesPotenzialInJProKg = (matrixpotenzialInCm / 100) * erdbeschleunigungInMProS2;
	const druckInhPa = (chemischesPotenzialInJProKg * wasserdichteInKgProm3) / hekto;
	return {
		chemischesPotenzialInJProKg,
		druckInhPa,
		matrixpotenzialInCm,
		pF
	};
};
