import { random } from '../highscore';

interface Feuchtefront {
	feuchtefrontInCm: number;
	finalerVolumetrischerLuftgehalt: number;
	finalerVolumetrischerWassergehalt: number;
	initialerVolumetrischerWassergehalt: number;
	lagerungsdichteInGProCm3: number;
	niederschlagInMm: number;
}

export const createFeuchtefront = (): Feuchtefront => {
	const festsubstanzdichte = 2.65;
	const niederschlagInMm = Math.round(random(20, 90));
	const initialerVolumetrischerWassergehalt = Math.round(random(30, 50)) / 100;
	const wassergehaltsaenderung = Math.round(random(10, 50 - 100 * initialerVolumetrischerWassergehalt)) / 100;
	const finalerVolumetrischerWassergehalt =
		initialerVolumetrischerWassergehalt + wassergehaltsaenderung;
	const finalerVolumetrischerLuftgehalt = 0.7 - finalerVolumetrischerWassergehalt;
	const porositaet = finalerVolumetrischerWassergehalt + finalerVolumetrischerLuftgehalt;
	const lagerungsdichteInGProCm3 = (1 - porositaet) * festsubstanzdichte;
	const feuchtefrontInCm = Math.round(niederschlagInMm / 10 / wassergehaltsaenderung);
	return {
		feuchtefrontInCm,
		finalerVolumetrischerLuftgehalt,
		finalerVolumetrischerWassergehalt,
		initialerVolumetrischerWassergehalt,
		lagerungsdichteInGProCm3,
		niederschlagInMm
	};
};
