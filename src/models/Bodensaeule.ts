import { arithmeticMean, random } from '../highscore';

interface Bodensaeule {
	makroporen: {
		anteilInProzent: number;
		wasserLeitfaehigkeitInCmProTag: number;
	};
	matrix: {
		anteilInProzent: number;
		wasserLeitfaehigkeitInCmProTag: number;
	};
	oberflaecheInCm2: number;
	wasserLeitfaehigkeitInCmProTag: number;
}

export const createBodensaeule = () => {
	const oberflaecheInCm2 = Math.round(random(10, 90));
	const makroporen = {
		anteilInProzent: Math.round(random(1, 10)),
		wasserLeitfaehigkeitInCmProTag: 100000
	};
	const matrix = {
		anteilInProzent: 100 - makroporen.anteilInProzent,
		wasserLeitfaehigkeitInCmProTag: Math.round(random(1, 100))
	};
	const wasserLeitfaehigkeitInCmProTag = arithmeticMean(
		[matrix, makroporen],
		({ wasserLeitfaehigkeitInCmProTag }) => wasserLeitfaehigkeitInCmProTag,
		({ anteilInProzent }) => anteilInProzent / 100
	);

	const bodensaeule: Bodensaeule = {
		makroporen,
		matrix,
		oberflaecheInCm2,
		wasserLeitfaehigkeitInCmProTag
	};
	return bodensaeule;
};
