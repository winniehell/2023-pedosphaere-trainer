import { random } from '../highscore';

interface Horizont {
	maechtigkeitInCm: number;
	name: string;
	volumetrischeWassergehalt: number;
	wasserLeitfaehigkeitInCmProTag: number;
	wasserMengeInMm: number;
}

export const createHorizont = (name: string) => {
	const maechtigkeitInCm = Math.round(random(10, 50));
	const volumetrischeWassergehalt = Math.round(random(15, 30)) / 100;
	const wasserMengeInMm = 10 * maechtigkeitInCm * volumetrischeWassergehalt;
	const wasserLeitfaehigkeitInCmProTag =  Math.round(random(1, 100));

	const horizont: Horizont = {
		maechtigkeitInCm,
		name,
		volumetrischeWassergehalt,
		wasserLeitfaehigkeitInCmProTag,
		wasserMengeInMm
	};
	return horizont;
};
