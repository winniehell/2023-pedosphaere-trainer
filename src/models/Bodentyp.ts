import {sample} from "../highscore";

interface Bodentyp {
    name: string;
    horizontNamen: string[];
}

export const createBodentyp = () => sample<Bodentyp>([
    {name: 'Braunerde', horizontNamen: ['Ap', 'Bv', 'C']},
    {name: 'Podsol', horizontNamen: ['Ah', 'Ae', 'Bs', 'Cv']},
    {name: 'Pseudogley', horizontNamen: ['Ah', 'Sw', 'Sd']},
])
