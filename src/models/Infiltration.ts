import { createStechzylinderProbe } from './StechzylinderProbe';
import { random } from '../highscore';

interface Infiltration {
	abstandsgeschwindigkeitInMmProTag: number;
	infiltrationsrateInMmProTag: number;
	lagerungsdichteInGProCm3: number;

	retardierungsfaktor: number;
	verlagerungsgeschwindigkeitInMmProTag: number;
	verteilungskoeffizientInCm3ProG: number;

	volumetrischerWassergehalt: number;
}

export const createInfiltration = (): Infiltration => {
	const { lagerungsdichteInGProCm3, volumetrischerWassergehalt } = createStechzylinderProbe();
	const verteilungskoeffizientInCm3ProG = Math.round(random(5, 50)) / 10;
	const infiltrationsrateInMmProTag = Math.round(random(10, 70));
	const abstandsgeschwindigkeitInMmProTag =
		infiltrationsrateInMmProTag / volumetrischerWassergehalt;
	const retardierungsfaktor =
		1 + (lagerungsdichteInGProCm3 / volumetrischerWassergehalt) * verteilungskoeffizientInCm3ProG;
	const verlagerungsgeschwindigkeitInMmProTag =
		Math.round(abstandsgeschwindigkeitInMmProTag) / Math.round(retardierungsfaktor);
	return {
		abstandsgeschwindigkeitInMmProTag,
		infiltrationsrateInMmProTag,
		lagerungsdichteInGProCm3,
		retardierungsfaktor,
		verlagerungsgeschwindigkeitInMmProTag,
		verteilungskoeffizientInCm3ProG,
		volumetrischerWassergehalt
	};
};
